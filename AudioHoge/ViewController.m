//
//  ViewController.m
//  AudioHoge
//
//  Created by pies on 2018/01/27.
//  Copyright © 2018年 pies. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

#import "ViewController.h"
#import "BufferManager.h"

#import "GLHogeView.h"

@interface ViewController () {
    
    BOOL isFirstLoad;

    AVAudioSession          *session;
    AVAudioEngine           *_engine;
    AVAudioUnitSampler      *_sampler;
    AVAudioUnitDistortion   *_distortion;
    AVAudioUnitReverb       *_reverb;
    AVAudioPlayerNode       *_player;
    
    // the sequencer
    AVAudioSequencer        *_sequencer;
    double                  _sequencerTrackLengthSeconds;
    
    // buffer for the player
    AVAudioPCMBuffer        *_playerLoopBuffer;
    
    // for the node tap
    NSURL                   *_mixerOutputFileURL;
    BOOL                    _isRecording;
    BOOL                    _isRecordingSelected;
    
    // mananging session and configuration changes
    BOOL                    _isSessionInterrupted;
    BOOL                    _isConfigChangePending;

    
    
    
    
}

@property (weak, nonatomic) IBOutlet UIView *containerGL1;

@property (nonatomic) BufferManager *bm;

@property (nonatomic) GLHogeView *hogeV1;



@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _bm = [BufferManager new];
    
    [self setupAudio];
    
    isFirstLoad = YES;
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (isFirstLoad) {
        isFirstLoad = NO;
        NSError *error;
        [_engine startAndReturnError:&error];
        if (error) {
            NSLog(@"Error engine startAndReturnError %@\n", [error localizedDescription]);
        }
        
        [self setupGL];
        
    }
}

- (void)setupAudio {
    [self setupAudioSession];
    [self createEngineAndAttachNodes];
    
}



- (void)setupAudioSession {
    
    // Configure the audio session
    session = [AVAudioSession sharedInstance];
    NSError *error;
    
    // set the session category
    bool success = [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:&error];
    if (!success) NSLog(@"Error setting AVAudioSession category! %@\n", [error localizedDescription]);

    // サンプルレート:44.1 kHz
    double hwSampleRate = 44100.0;
    success = [session setPreferredSampleRate:hwSampleRate error:&error];
    if (!success) NSLog(@"Error setting preferred sample rate! %@\n", [error localizedDescription]);
    
    NSTimeInterval ioBufferDuration = 0.0029;
    success = [session setPreferredIOBufferDuration:ioBufferDuration error:&error];
    if (!success) NSLog(@"Error setting preferred io buffer duration! %@\n", [error localizedDescription]);

    // 計測モードにする
    [session setMode:AVAudioSessionModeMeasurement error:&error];
    
    // 割り込み系のコールバックはとりあえず実装しない
    // activate the audio session
//    success = [session setActive:NO error:&error];
    success = [session setActive:YES error:&error];
    if (!success) NSLog(@"Error setting session active! %@\n", [error localizedDescription]);
    
}

- (void)createEngineAndAttachNodes
{
    NSError *error;
    BOOL success = NO;

    _engine = nil;
    _sampler = nil;
    _distortion = nil;
    _reverb = nil;
    _player = nil;
    
    
    _player = [[AVAudioPlayerNode alloc] init];
    
    _sampler = [[AVAudioUnitSampler alloc] init];
    
    
//    // load drumloop into a buffer for the playernode
//    NSURL *drumLoopURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"drumLoop" ofType:@"caf"]];
//    AVAudioFile *drumLoopFile = [[AVAudioFile alloc] initForReading:drumLoopURL error:&error];
//    _playerLoopBuffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:[drumLoopFile processingFormat] frameCapacity:(AVAudioFrameCount)[drumLoopFile length]];
//    success = [drumLoopFile readIntoBuffer:_playerLoopBuffer error:&error];
//    NSAssert(success, @"couldn't read drumLoopFile into buffer, %@", [error localizedDescription]);
    
    _isRecording = NO;
    _isRecordingSelected = NO;
    
    _engine = [AVAudioEngine new];
    
   AVAudioMixerNode *mixer = [_engine mainMixerNode];
    AVAudioInputNode *input = [_engine inputNode];
    
    
    [_engine connect:input to:mixer format:[input inputFormatForBus:0]];


    __block __weak typeof(self) wSelf = self;
    
    
    [input installTapOnBus:0 bufferSize:1024 format:[input outputFormatForBus:0] block:^(AVAudioPCMBuffer *buffer, AVAudioTime *when) {
        
        // 1フレーム:1024hz サンプルレート:44.1kHz -> (1 / 44100) * (44100 / 1024) -> 0.0009???
        buffer.frameLength = 1024;
        Float32 *audioData = buffer.audioBufferList->mBuffers[0].mData;
        
        if (audioData != NULL) {
            [wSelf.bm copyAudioDataToDrawBuffer:audioData frame:buffer.frameLength];
            for (UInt32 i=0; i<buffer.frameLength; i++) {
                NSLog(@"indata[%d]:%f\n",i,audioData[i]);
            }
        }
        //buffer.audioBufferList->mBuffers[0].mData
//cd.bufferManager->CopyAudioDataToDrawBuffer((Float32*)ioData->mBuffers[0].mData, inNumberFrames);
//        void BufferManager::CopyAudioDataToDrawBuffer( Float32* inData, UInt32 inNumFrames )
//        {
//            if (inData == NULL) return;
//
//            for (UInt32 i=0; i<inNumFrames; i++)
//            {
//                if ((i+mDrawBufferIndex) >= mCurrentDrawBufferLen)
//                {
//                    CycleDrawBuffers();
//                    mDrawBufferIndex = -i;
//                }
//                mDrawBuffers[0][i + mDrawBufferIndex] = inData[i];
//                printf("indata[%d]:%f\n",i,inData[i]);
        
//        for (UInt32 i=0; i<512; i++)
//        {
//            if ((i+mDrawBufferIndex) >= mCurrentDrawBufferLen)
//            {
//                CycleDrawBuffers();
//                mDrawBufferIndex = -i;
//            }
//            mDrawBuffers[0][i + mDrawBufferIndex] = inData[i];
//            printf("indata[%d]:%f\n",i,inData[i]);
//            //        cout << "indata[" << i  << inData[i] << "]" << endl;
//            //p
//        }
//        mDrawBufferIndex += inNumFrames;

        
        
        //buffer.audioBufferList->mBuffers
//        buffer.frameLength = 1024; //here

//        NSLog(@"....");
//        [input installTapOnBus:0 bufferSize:1024 format:[mixer outputFormatForBus:0] block:^(AVAudioPCMBuffer *buffer, AVAudioTime *when) {
//            buffer.frameLength = 1024; //here
//        for (UInt32 i = 0; i < buffer.audioBufferList->mNumberBuffers; i++) {
//            Float32 *data = buffer.audioBufferList->mBuffers[i].mData;
//            UInt32 frames = buffer.audioBufferList->mBuffers[i].mDataByteSize / sizeof(Float32);
//
//        }
    }];
    
    [_engine startAndReturnError:&error];
    
    
    /*  To support the instantiation of arbitrary AVAudioNode subclasses, instances are created
     externally to the engine, but are not usable until they are attached to the engine via
     the attachNode method. */
    
//    [_engine attachNode:_sampler];
//    [_engine attachNode:_distortion];
//    [_engine attachNode:_reverb];
//    [_engine attachNode:_player];
//    [_engine st]
    
}

//// Render callback function
//static OSStatus    performRender (void                         *inRefCon,
//                                  AudioUnitRenderActionFlags     *ioActionFlags,
//                                  const AudioTimeStamp         *inTimeStamp,
//                                  UInt32                         inBusNumber,
//                                  UInt32                         inNumberFrames,
//                                  AudioBufferList              *ioData)
//{
//    OSStatus err = noErr;
//    if (*cd.audioChainIsBeingReconstructed == NO)
//    {
//        // we are calling AudioUnitRender on the input bus of AURemoteIO
//        // this will store the audio data captured by the microphone in ioData
//        err = AudioUnitRender(cd.rioUnit, ioActionFlags, inTimeStamp, 1, inNumberFrames, ioData);
//
//        // filter out the DC component of the signal
//        cd.dcRejectionFilter->ProcessInplace((Float32*) ioData->mBuffers[0].mData, inNumberFrames);
//
//        // based on the current display mode, copy the required data to the buffer manager
//        if (cd.bufferManager->GetDisplayMode() == aurioTouchDisplayModeOscilloscopeWaveform)
//        {
//            cd.bufferManager->CopyAudioDataToDrawBuffer((Float32*)ioData->mBuffers[0].mData, inNumberFrames);
//        }
//
//        else if ((cd.bufferManager->GetDisplayMode() == aurioTouchDisplayModeSpectrum) || (cd.bufferManager->GetDisplayMode() == aurioTouchDisplayModeOscilloscopeFFT))
//        {
//            if (cd.bufferManager->NeedsNewFFTData())
//            cd.bufferManager->CopyAudioDataToFFTInputBuffer((Float32*)ioData->mBuffers[0].mData, inNumberFrames);
//        }
//
//        // mute audio if needed
//        if (*cd.muteAudio)
//        {
//            for (UInt32 i=0; i<ioData->mNumberBuffers; ++i)
//            memset(ioData->mBuffers[i].mData, 0, ioData->mBuffers[i].mDataByteSize);
//        }
//    }
//
//    return err;
//}
//
//
//
//
//
//
//
- (void)setupGL {

    CGRect rct = _containerGL1.frame;
    rct.origin = CGPointZero;

    _hogeV1 = [[GLHogeView alloc] initWithFrame:rct];
    _hogeV1.bm = _bm;

    [_containerGL1 addSubview: _hogeV1];
    [_containerGL1 bringSubviewToFront:_hogeV1];

    [_hogeV1 startAnimation];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
