//
//  AppDelegate.h
//  AudioHoge
//
//  Created by pies on 2018/01/27.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

