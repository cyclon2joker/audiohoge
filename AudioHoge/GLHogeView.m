//
//  GLHogeView.m
//  AudioHoge
//
//  Created by pies on 2018/01/27.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/EAGLDrawable.h>

#import "GLHogeView.h"

#define USE_DEPTH_BUFFER 1
#define SPECTRUM_BAR_WIDTH 4
#define kMinDrawSamples 64
#define kMaxDrawSamples 4096


#ifndef CLAMP
#define CLAMP(min,x,max) (x < min ? min : (x > max ? max : x))
#endif


// TODO from buffermanager
const UInt32 kNumDrawBuffers = 12;
const UInt32 kDefaultDrawSamples = 1024;




@interface GLHogeView() {

    /* The pixel dimensions of the backbuffer */
    GLint backingWidth;
    GLint backingHeight;
    
    EAGLContext *context;
    
    /* OpenGL names for the renderbuffer and framebuffers used to render to this view */
    GLuint viewRenderbuffer, viewFramebuffer;
    
    /* OpenGL name for the depth buffer that is attached to viewFramebuffer, if it exists (0 if it does not exist) */
    GLuint depthRenderbuffer;
    
    NSTimer                     *animationTimer;
    NSTimeInterval              animationInterval;
    NSTimeInterval              animationStarted;
    
    BOOL                        applicationResignedActive;
    
    UIImageView*                sampleSizeOverlay;
    UILabel*                    sampleSizeText;
    
    BOOL                        initted_oscilloscope, initted_spectrum;
    UInt32*                        texBitBuffer;
    CGRect                        spectrumRect;
    
    GLuint                        bgTexture;
    GLuint                        muteOffTexture, muteOnTexture;
    GLuint                        fftOffTexture, fftOnTexture;
    GLuint                        sonoTexture;
    
//    aurioTouchDisplayMode        displayMode;
//
//    SpectrumLinkedTexture*        firstTex;
    
    UIEvent*                    pinchEvent;
    CGFloat                        lastPinchDist;
    Float32*                    l_fftData;
    GLfloat*                    oscilLine;
//    AudioController*            audioController;
    
}




@end


@implementation GLHogeView

+ (Class) layerClass
{
    return [CAEAGLLayer class];
}


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _applicationResignedActive = NO;
        
        // Get the layer
        CAEAGLLayer *eaglLayer = (CAEAGLLayer*) self.layer;
        
        eaglLayer.opaque = YES;
        
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithBool:FALSE],
                                        kEAGLDrawablePropertyRetainedBacking,
                                        kEAGLColorFormatRGBA8,
                                        kEAGLDrawablePropertyColorFormat,
                                        nil];
        
        context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
        if(!context || ![EAGLContext setCurrentContext:context] || ![self createFramebuffer]) {
            return nil;
        }

        oscilLine = (GLfloat*)malloc(1024 * 2 * sizeof(GLfloat));
        animationInterval = 1.0 / 60.0;
        
        [self setupView];
//        [self drawView];

//        displayMode = aurioTouchDisplayModeOscilloscopeWaveform;
//
//        // Set up our overlay view that pops up when we are pinching/zooming the oscilloscope
//        UIImage *img_ui = nil;
//        {
//            // Draw the rounded rect for the bg path using this convenience function
//            CGPathRef bgPath = CreateRoundedRectPath(CGRectMake(0, 0, 110, 234), 15.);
//
//            CGColorSpaceRef cs = CGColorSpaceCreateDeviceRGB();
//            // Create the bitmap context into which we will draw
//            CGContextRef cxt = CGBitmapContextCreate(NULL, 110, 234, 8, 4*110, cs, kCGImageAlphaPremultipliedFirst);
//            CGContextSetFillColorSpace(cxt, cs);
//            CGFloat fillClr[] = {0., 0., 0., 0.7};
//            CGContextSetFillColor(cxt, fillClr);
//            // Add the rounded rect to the context...
//            CGContextAddPath(cxt, bgPath);
//            // ... and fill it.
//            CGContextFillPath(cxt);
//
//            // Make a CGImage out of the context
//            CGImageRef img_cg = CGBitmapContextCreateImage(cxt);
//            // Make a UIImage out of the CGImage
//            img_ui = [UIImage imageWithCGImage:img_cg];
//
//            // Clean up
//            CGImageRelease(img_cg);
//            CGColorSpaceRelease(cs);
//            CGContextRelease(cxt);
//            CGPathRelease(bgPath);
//        }
        
//        // Create the image view to hold the background rounded rect which we just drew
//        sampleSizeOverlay = [[UIImageView alloc] initWithImage:img_ui];
//        sampleSizeOverlay.frame = CGRectMake(190, 124, 110, 234);
//
//        // Create the text view which shows the size of our oscilloscope window as we pinch/zoom
//        sampleSizeText = [[UILabel alloc] initWithFrame:CGRectMake(-62, 0, 234, 234)];
//        sampleSizeText.textAlignment = NSTextAlignmentCenter;
//        sampleSizeText.textColor = [UIColor whiteColor];
//        sampleSizeText.text = NSLocalizedString(@"0000 ms", nil);
//        sampleSizeText.font = [UIFont boldSystemFontOfSize:36.];
//        // Rotate the text view since we want the text to draw top to bottom (when the device is oriented vertically)
//        sampleSizeText.transform = CGAffineTransformMakeRotation(M_PI_2);
//        sampleSizeText.backgroundColor = [UIColor clearColor];
//
//        // Add the text view as a subview of the overlay BG
//        [sampleSizeOverlay addSubview:sampleSizeText];
//        // Text view was retained by the above line, so we can release it now
//        [sampleSizeText release];
        
        // We don't add sampleSizeOverlay to our main view yet. We just hang on to it for now, and add it when we
        // need to display it, i.e. when a user starts a pinch/zoom.

        
        // Set up the view to refresh at 20 hz
//        [self setAnimationInterval:1./20.];
//        [self startAnimation];

        
        
        
        
        
    }
    return self;
}

- (void)layoutSubviews
{
    [EAGLContext setCurrentContext:context];
    [self destroyFramebuffer];
    [self createFramebuffer];
    [self drawView];
}


- (BOOL)createFramebuffer
{
    // フレームバッファ・レンダーバッファ準備
    glGenFramebuffersOES(1, &viewFramebuffer);
    glGenRenderbuffersOES(1, &viewRenderbuffer);
    
    glBindFramebufferOES(GL_FRAMEBUFFER_OES, viewFramebuffer);
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, viewRenderbuffer);
    [context renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:(id<EAGLDrawable>)self.layer];
    glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, viewRenderbuffer);
    
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth);
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight);
    
    if(USE_DEPTH_BUFFER) {
        glGenRenderbuffersOES(1, &depthRenderbuffer);
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, depthRenderbuffer);
        glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, backingWidth, backingHeight);
        glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer);
    }
    
    if(glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES) {
        NSLog(@"failed to make complete framebuffer object %x", glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES));
        return NO;
    }
    
    return YES;
}

- (void)destroyFramebuffer
{
    glDeleteFramebuffersOES(1, &viewFramebuffer);
    viewFramebuffer = 0;
    glDeleteRenderbuffersOES(1, &viewRenderbuffer);
    viewRenderbuffer = 0;
    
    if(depthRenderbuffer) {
        glDeleteRenderbuffersOES(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
}


- (void)setupView
{
    // Sets up matrices and transforms for OpenGL ES
    glViewport(0, 0, backingWidth, backingHeight);
    // 投影モード設定
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(0, 300, -150, 150, -1.0f, 1.0f);
//    void glOrthof(    GLfloat left,
//                  GLfloat right,
//                  GLfloat bottom,
//                  GLfloat top,
//                  GLfloat near,
//                  GLfloat far);
    
    // 描画モード
    glMatrixMode(GL_MODELVIEW);
    // Clears the view with black
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnableClientState(GL_VERTEX_ARRAY);
    
}

// Updates the OpenGL view when the timer fires
- (void)drawView
{
    // the NSTimer seems to fire one final time even though it's been invalidated
    // so just make sure and not draw if we're resigning active
    //if (self.applicationResignedActive) return;
    
    // Make sure that you are drawing to the current context
    [EAGLContext setCurrentContext:context];
    
    glBindFramebufferOES(GL_FRAMEBUFFER_OES, viewFramebuffer);
    
    [self drawView:self forTime:([NSDate timeIntervalSinceReferenceDate] - animationStarted)];
    
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, viewRenderbuffer);
    [context presentRenderbuffer:GL_RENDERBUFFER_OES];
}

- (void)drawView:(id)sender forTime:(NSTimeInterval)time
{
    [self drawOscilloscope];
//    if (![audioController audioChainIsBeingReconstructed])  //hold off on drawing until the audio chain has been reconstructed
//    {
//        if ((displayMode == aurioTouchDisplayModeOscilloscopeWaveform) || (displayMode == aurioTouchDisplayModeOscilloscopeFFT))
//        {
//            if (!initted_oscilloscope) [self setupViewForOscilloscope];
//            [self drawOscilloscope];
//        } else if (displayMode == aurioTouchDisplayModeSpectrum) {
//            if (!initted_spectrum) [self setupViewForSpectrum];
//            [self drawSpectrum];
//        }
//    }
}

- (void)drawOscilloscope
{
    
    // 色をセット
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    // カラーバッファを塗りつぶす
    glClear(GL_COLOR_BUFFER_BIT);
    
    // 多分、音声データ取り出し。とりあえず、コメントアウト
//    BufferManager* bufferManager = [audioController getBufferManagerInstance];
//    Float32** drawBuffers = bufferManager->GetDrawBuffers();
    
    Float32 *drawBuffer = [_bm getDrawBuffer];
    if (drawBuffer == NULL) {
        return;
    }
    GLfloat *oscilLine_ptr = oscilLine;
    GLfloat max = 1024; //bufferManager->GetCurrentDrawBufferLength();

    // 座標設定
    for (int i=0; i<1024; i++)
    {
        oscilLine[i * 2] = i * (300. / 1024.);
        oscilLine[(i*2 +1)] = ((Float32)(drawBuffer[i]) * 500.);
        NSLog(@":::%d::%f,%F",i,oscilLine[i] , oscilLine[(i*2 +1)]);
    }

    glEnableClientState(GL_VERTEX_ARRAY); //有効化
    glLineWidth(2.);
    glColor4f(0., 1., 0., 0.5);
    // Set up vertex pointer,
    glVertexPointer(2, GL_FLOAT, 0, oscilLine);
    glDrawArrays(GL_LINE_STRIP, 0, 1024);
    
    //[self stopAnimation];
    // とりあえず、FFTは一旦しない
//    if (displayMode == aurioTouchDisplayModeOscilloscopeFFT)
//    {
//        if (bufferManager->HasNewFFTData())
//        {
//            bufferManager->GetFFTOutput(l_fftData);
//
//            int y, maxY;
//            maxY = bufferManager->GetCurrentDrawBufferLength();
//            int fftLength = bufferManager->GetFFTOutputBufferLength();
//            for (y=0; y<maxY; y++)
//            {
//                CGFloat yFract = (CGFloat)y / (CGFloat)(maxY - 1);
//                CGFloat fftIdx = yFract * ((CGFloat)fftLength - 1);
//
//                double fftIdx_i, fftIdx_f;
//                fftIdx_f = modf(fftIdx, &fftIdx_i);
//
//                CGFloat fft_l_fl, fft_r_fl;
//                CGFloat interpVal;
//
//                int lowerIndex = (int) fftIdx_i;
//                int upperIndex = (int) fftIdx_i + 1;
//                upperIndex = (upperIndex == fftLength) ? fftLength - 1 : upperIndex;
//
//                fft_l_fl = (CGFloat)(l_fftData[lowerIndex] + 80) / 64.;
//                fft_r_fl = (CGFloat)(l_fftData[upperIndex] + 80) / 64.;
//                interpVal = fft_l_fl * (1. - fftIdx_f) + fft_r_fl * fftIdx_f;
//
//                drawBuffers[0][y] = CLAMP(0., interpVal, 1.);
//            }
//            [self cycleOscilloscopeLines];
//        }
//    }
    
    
//    glPushMatrix();
    
    // Translate to the left side and vertical center of the screen, and scale so that the screen coordinates
    // go from 0 to 1 along the X, and -1 to 1 along the Y
//    glTranslatef(17., 182., 0.);
//    glScalef(448., 116., 1.);
    
    // Set up some GL state for our oscilloscope lines
//    glDisable(GL_TEXTURE_2D);
//    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
//    glDisableClientState(GL_COLOR_ARRAY);
    
    
/*
    glDisable(GL_LINE_SMOOTH);
    glLineWidth(2.);
    
    

    GLfloat    vertices[] = {
        -1.0,    -1.0,    0.0,
        1.0,    -1.0,    0.0,
        1.0,     1.0,    0.0,
        -1.0,     1.0,    0.0,
        -1.0,    -1.0,    0.0,
    };
    GLfloat    vertices2[] = {
        0.0,    0.0,
        0.0,    100.0,
        300.0,     150.0
    };
    GLfloat colors2[] = {
        0.,    1.,    0.,    1.,
        0.,    1.,    0.,    1.,
        0.,    1.,    0.,    1.
    };
    
//    glColor4f(1., 1., 1., 1.);
    glEnableClientState(GL_VERTEX_ARRAY); //有効化
    glLineWidth(5.);
//    glColor4f(0., 1., 0., 1.);
//    glColorPointer(4 , GL_FLOAT, 0, colors2);
    glColor4f(0., 1., 0., 0.5);
    glVertexPointer(2, GL_FLOAT, 0, vertices2);
    glDrawArrays( GL_LINE_STRIP , 0, 3 );

   */
//    glPushMatrix();
//        glPopMatrix();
//        glPopMatrix();

//    glVertexPointer(3, GL_FLOAT, 0, vertices);
//    glDrawArrays( GL_LINE_STRIP , 0, 5 );
    
    
//    glPopMatrix();
//    glPopMatrix();
}

- (void)startAnimation
{
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:animationInterval target:self selector:@selector(drawView) userInfo:nil repeats:YES];
    animationStarted = [NSDate timeIntervalSinceReferenceDate];
}


- (void)stopAnimation
{
    [animationTimer invalidate];
    animationTimer = nil;
}


- (void)setAnimationInterval:(NSTimeInterval)interval
{
    animationInterval = interval;
//    if(animationTimer) {
//        [self stopAnimation];
//        [self startAnimation];
//    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
