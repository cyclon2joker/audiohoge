//
//  BufferManager.m
//  AudioHoge
//
//  Created by pies on 2018/02/04.
//  Copyright © 2018年 pies. All rights reserved.
//
#include <AudioToolbox/AudioToolbox.h>
#include <libkern/OSAtomic.h>

#import "BufferManager.h"

@interface BufferManager() {
    
    Float32*        mDrawBuffer;
    UInt32          mDrawBufferIndex;
    UInt32          mCurrentDrawBufferLen;

    
}

@end

@implementation BufferManager

- (id)init{
    self = [super init];
    
    mDrawBuffer = (Float32*) calloc(4096, sizeof(Float32));

    
    
    return self;
}


-(void)copyAudioDataToDrawBuffer:(Float32*)inData frame:(UInt32)frame
{
    if (inData == NULL) {
        return;
    }
    for (UInt32 i=0; i<frame; i++)
    {
        mDrawBuffer[i] = inData[i];
    }
}
- (Float32*)getDrawBuffer
{
    return mDrawBuffer;
}




@end
